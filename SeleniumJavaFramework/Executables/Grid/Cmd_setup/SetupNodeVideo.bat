pushd %~dp0%..\..\..\lib
set SELSERV="selenium-server-standalone-*.jar"
for /f %%i in (%SELSERV%) do set SELSERV=%%~fi

start java -cp selenium-video-node-1.9.jar;"%SELSERV%" org.openqa.grid.selenium.GridLauncher -role webdriver -nodeConfig ..\ConfigFiles\NodeConfig.json