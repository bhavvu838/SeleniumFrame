IF NOT "%ProgFiles86Root%"=="" GOTO amd64
::SET SIKULIX_HOME=%~dp0%..\..\Resources\SikuliX86
:amd64
:: Set Variables 

set ProjectPath=%1

echo %ProjectPath%

::set ProjectPath=D:\SeleniumJavaFramework

set ANT_HOME=%ProjectPath%\Resources\ant
echo %ANT_HOME%

set PATH=%PATH%;%ANT_HOME%\bin;%ProjectPath%\Resources\allure-cli\bin
set JDK="%ProgramFiles%\Java\jdk*"
for /d %%i in (%JDK%) do set JAVA_HOME=%%i
:: Display Variables and Launch Ant
set JAVA_HOME
echo %ANT_HOME%
echo %PATH%
pushd %ProjectPath%
call ant clean
Call ant compile

