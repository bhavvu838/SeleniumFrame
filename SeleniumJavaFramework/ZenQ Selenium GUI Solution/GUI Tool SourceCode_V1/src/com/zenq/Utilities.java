/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zenq;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 *
 * @author Yatheesh
 */
public class Utilities {
     public Set<String> groups=new HashSet<String>();
     String comPath="";
     
     
    private static void copyFileUsingFileChannels(File source, File dest) throws IOException {
        FileChannel inputChannel = null;
        FileChannel outputChannel = null;
        try {
            inputChannel = new FileInputStream(source).getChannel();
            outputChannel = new FileOutputStream(dest).getChannel();
            outputChannel.transferFrom(inputChannel, 0, inputChannel.size());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            inputChannel.close();
            outputChannel.close();
        }
    }
    
    public static boolean isNumb(String str) {
        return str.matches("\\d+");
    }
    
    public void createProjectJar(String projectComPackagePath,String jarPath) {
        JarOutputStream target = null;
        Utilities util = new Utilities();
        try {
            Manifest manifest = new Manifest();
            manifest.getMainAttributes().put(Attributes.Name.MANIFEST_VERSION, "1.0");
            //target = new JarOutputStream(new FileOutputStream("D:\\SeleniumUI\\ExecutableJarTesting\\lib\\SeleniumProjectBuild.jar"), manifest);
            target = new JarOutputStream(new FileOutputStream(jarPath), manifest);
           // util.setRequiredDirectory("D:\\SeleniumJavaFramework\\SeleniumJavaFramework\\build", 3);
            String comPath=util.setRequiredDirectory(projectComPackagePath, 3);
            util.createJar(new File(comPath), target);
            target.close();
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(com.testsuite.Test1.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(com.testsuite.Test1.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                target.close();
            } catch (IOException ex) {
                //Logger.getLogger(com.testsuite.Test1.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void createJar(File source, JarOutputStream target) throws IOException {
        BufferedInputStream in = null;
        try {
            String abc = source.getPath();
            if (source.isDirectory()) {
                int indexOfCom = abc.indexOf("com");
                String finalString = abc.substring(indexOfCom, abc.length());
                finalString.replace("\\", "/");
                if (!finalString.isEmpty()) {
                    if (!finalString.endsWith("/")) {
                        finalString += "/";
                    }
                    JarEntry entry = new JarEntry(finalString);
                    entry.setTime(source.lastModified());
                    target.putNextEntry(entry);
                    target.closeEntry();
                }
                for (File nestedFile : source.listFiles()) {
                    createJar(nestedFile, target);
                }
                return;
            }
            int indexOfCom = abc.indexOf("com");
            String finalString = abc.substring(indexOfCom, abc.length());
            finalString.replace("\\", "/");
            JarEntry entry = new JarEntry(finalString.replace("\\", "/"));
            entry.setTime(source.lastModified());
            target.putNextEntry(entry);
            in = new BufferedInputStream(new FileInputStream(source));

            byte[] buffer = new byte[1024];
            while (true) {
                int count = in.read(buffer);
                if (count == -1) {
                    break;
                }
                target.write(buffer, 0, count);
            }
            target.closeEntry();
        } finally {
            if (in != null) {
                in.close();
            }
        }
    }
  
    public String setRequiredDirectory(String dirPath, int level) throws IOException {
        File dir = new File(dirPath);
        File[] firstLevelFiles = dir.listFiles();
        if (firstLevelFiles != null && firstLevelFiles.length > 0) {
            for (File aFile : firstLevelFiles) {
                for (int i = 0; i < level; i++) {
                    System.out.print(" ");
                }
                if (aFile.isDirectory()) {
                    if (aFile.getAbsolutePath().contains("com")) {
                        comPath = aFile.getAbsolutePath();
                        break;
                    }
                    setRequiredDirectory(aFile.getAbsolutePath(), level + 1);
                } else {
                   
                }
            }
        }        
        System.out.println();
        return comPath;
    }
    public Set<String> extractGroupsFromTheProject(){
        ZipInputStream zip = null;
        try {            
            List<String> classNames = new ArrayList<String>();
            Path workingDirectory=Paths.get(".").toAbsolutePath();
            String jarPath=workingDirectory.toString().replace("\\.", "")+"\\lib\\SeleniumProjectBuild.jar";
            System.out.println("SeleniumProjectBuild.jar Path:"+jarPath);
            zip = new ZipInputStream(new FileInputStream(jarPath));
            if(zip!=null){
                for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
                    if (!entry.isDirectory() && entry.getName().endsWith(".class")) {
                        String fullPath=entry.getName();
                        
                        int indexOfCom=fullPath.indexOf("com");
                        String packagePath=fullPath.substring(indexOfCom, fullPath.length());
                       
                        // This ZipEntry represents a class. Now, what class does it represent?
                        String className = packagePath.replace('/', '.'); // including ".class"
                        classNames.add(className.substring(0, className.length() - ".class".length()));
                    }
                }
                for(String clas:classNames){
                     if(clas.contains("testsuite.")){
                         getGroupsAnnotatedWithTest(Class.forName(clas));
                     }
                }
                return groups;
            }
            else{
                  System.out.println("files couldnot be extracted from SeleniumProjectBuild.jar, zip is null");  
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                zip.close();
            } catch (IOException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return groups;
    }
    
    public List<Method> getGroupsAnnotatedWithTest(final Class<?> type) {
        final List<Method> methods = new ArrayList<Method>();
        Class<?> klass = type;
        while (klass != Object.class) { // need to iterated thought hierarchy in order to retrieve methods from above the current instance
            // iterate though the list of methods declared in the class represented by klass variable, and add those annotated with the specified annotation
            final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
            for (final Method method : allMethods) {
                if (method.isAnnotationPresent(Test.class)) {
                    Test annotInstance = method.getAnnotation(Test.class);
                    String[] grps=annotInstance.groups();
                    
                    for(String s:grps){
                        groups.add(s);
                    }
                }
            }
            // move to the upper class in the hierarchy in search for more methods
            klass = klass.getSuperclass();
        }
        return methods;
    }
    
    public List<String> getMethodsAnnotatedWithTest(final Class<?> type) {
        final List<String> methods = new ArrayList<String>();
        Class<?> klass = type;
        while (klass != Object.class) { // need to iterated thought hierarchy in order to retrieve methods from above the current instance
            // iterate though the list of methods declared in the class represented by klass variable, and add those annotated with the specified annotation
            final List<Method> allMethods = new ArrayList<Method>(Arrays.asList(klass.getDeclaredMethods()));
            for (final Method method : allMethods) {
                if (method.isAnnotationPresent(Test.class)) {
                   methods.add(method.getName());
                }
            }
            // move to the upper class in the hierarchy in search for more methods
            klass = klass.getSuperclass();
        }
        return methods;
    }
    
     public List<String> extractPackages(String jarPath) {
        List<String> packages=new ArrayList<String>();
        ZipInputStream zip = null;
        try {
            List<String> classNames = new ArrayList<String>();
            Path workingDirectory = Paths.get(".").toAbsolutePath();
                     
            zip = new ZipInputStream(new FileInputStream(jarPath));
            if (zip != null) {
                for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
                    if(entry.isDirectory()){                       
                        if(entry.getName().contains("testsuite\\")){
                            String packge=entry.getName().replace("/", "");
                            packages.add(packge.replace("\\", "."));
                        }
                    }
                }             
            } else {
                System.out.println("files couldnot be extracted from SeleniumProjectBuild.jar, zip is null");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                zip.close();
            } catch (IOException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return packages;
          
    }
       
    public List<String> extractClasses(String jarPath,String packageName) {
        ZipInputStream zip = null;
        List<String> classes=new ArrayList<String>();
        try {
            List<String> classNames = new ArrayList<String>();
            Path workingDirectory = Paths.get(".").toAbsolutePath();
            zip = new ZipInputStream(new FileInputStream(jarPath));
            if (zip != null) {
                for (ZipEntry entry = zip.getNextEntry(); entry != null; entry = zip.getNextEntry()) {
                    if (entry.getName().contains(packageName)) {
                        String fullPath = entry.getName();                        
                        String className=fullPath.replace(packageName+"/", "");
                        classes.add(className);
                    }
                }
            } else {
                System.out.println("files couldnot be extracted from SeleniumProjectBuild.jar, zip is null");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                zip.close();
            } catch (IOException ex) {
                Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return classes;
    }
    
    //This method will update the newly generated testng-suite.xml file in Ant build.xml file from selenium project
    public void updateXmlIn_ANTBuildXmlFile(String testSuiteName,String xml){
         try {
             DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
             DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
             Document doc = docBuilder.parse(xml);
             
             //Get the root element
             Node company = doc.getFirstChild();
             
             // Get the staff element , it may not working if tag has spaces, or
             // whatever weird characters in front...it's better to use
             // getElementsByTagName() to get it directly.
             // Node staff = company.getFirstChild();
             
             // Get the staff element by tag name directly
             Node staff = doc.getElementsByTagName("xmlfileset").item(0);
             
             // update staff attribute
             NamedNodeMap attr = staff.getAttributes();
             Node nodeAttr = attr.getNamedItem("includes");
             //nodeAttr.setTextContent(testSuites.getSelectedItem().toString());
             nodeAttr.setTextContent(testSuiteName);
             
             // write the content into xml file
             TransformerFactory transformerFactory = TransformerFactory.newInstance();
             Transformer transformer = transformerFactory.newTransformer();
             DOMSource source = new DOMSource(doc);
             StreamResult result = new StreamResult(new File(xml));
             transformer.transform(source, result);

             System.out.println("Done");

         } catch (ParserConfigurationException ex) {
             Logger.getLogger(RunExistingTestSuite.class.getName()).log(Level.SEVERE, null, ex);
         } catch (SAXException ex) {
             Logger.getLogger(RunExistingTestSuite.class.getName()).log(Level.SEVERE, null, ex);
         } catch (IOException ex) {
             Logger.getLogger(RunExistingTestSuite.class.getName()).log(Level.SEVERE, null, ex);
         } catch (TransformerConfigurationException ex) {
             Logger.getLogger(RunExistingTestSuite.class.getName()).log(Level.SEVERE, null, ex);
         } catch (TransformerException ex) {
             Logger.getLogger(RunExistingTestSuite.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    //this method will invoke Ant_Build.bat file in the selenium framework
    public void runAntBuildBatchFile(){
        try {
            String antBuild_BatchFile=new ProjectPaths().getAntBuildBatFile();          
            Runtime.getRuntime().exec("cmd /C start \"\" \""+antBuild_BatchFile+"\"");
        } catch (IOException ex) {
            Logger.getLogger(Utilities.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void writeToPropertiesFile(String propertiesFile,String key, String data) {
        FileOutputStream fileOut = null;
        FileInputStream fileIn = null;
        try {
            Properties configProperty = new Properties();

            File file = new File(propertiesFile);
            
            fileIn = new FileInputStream(file);
            configProperty.load(fileIn);
            configProperty.setProperty(key, data);
            fileOut = new FileOutputStream(file);
            configProperty.store(fileOut, "sample properties");

        } catch (Exception ex) {
           // Logger.getLogger(WritePropertiesFile.class.getName()).log(Level.SEVERE, null, ex);
        } finally {

            try {
                fileOut.close();
            } catch (IOException ex) {
               // Logger.getLogger(WritePropertiesFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public String getValueFromPropFile(String propertiesFile, String key){     
       String reqString="";
        try {
            File file = new File(propertiesFile);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            reqString=(String)properties.get(key);
            fileInput.close();
            return reqString;
	} catch (FileNotFoundException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
	return null;	
    }
    
    public boolean isKeyExists(String propertiesFile,String key){
         String reqString="";
        try {
            File file = new File(propertiesFile);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            if(properties.containsKey(key))
                return true;
            else
                return false;
        
	} catch (FileNotFoundException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
	return false;	
    }
    
    public boolean removeKey(String propertiesFile,String key){
     String reqString="";
        try {
            File file = new File(propertiesFile);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            properties.remove(key);
	} catch (FileNotFoundException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
	return false;	
    }
    
    public void updatePropertiesFile(String propertiesFile,String key,String value){
        String reqString="";
        try {   
            
            FileInputStream in = new FileInputStream(propertiesFile);
            Properties props = new Properties();
            props.load(in);
            in.close();            
            
           // File file = new File(propertiesFile);
            FileOutputStream  fileOutput = new FileOutputStream (propertiesFile);                    
            props.setProperty(key, value);
            props.store(fileOutput, null);
            fileOutput.close();
            
	} catch (FileNotFoundException e) {
            e.printStackTrace();
	} catch (IOException e) {
            e.printStackTrace();
	}
    }
    
    
    public void clearPropertiesFile(String propertiesFile){
       FileOutputStream fileOut = null;
       FileInputStream fileIn = null;
       try {
           Properties configProperty = new Properties();
           File file = new File(propertiesFile);            
           fileIn = new FileInputStream(file);
           configProperty.load(fileIn);           
           configProperty.clear();           
          
           fileOut = new FileOutputStream(file);          
           configProperty.store(fileOut, null);

       } catch (Exception ex) {
          //Logger.getLogger(WritePropertiesFile.class.getName()).log(Level.SEVERE, null, ex);
       } finally {
           try {
               fileOut.close();
           } catch (IOException ex) {
              //Logger.getLogger(WritePropertiesFile.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
    }
}
