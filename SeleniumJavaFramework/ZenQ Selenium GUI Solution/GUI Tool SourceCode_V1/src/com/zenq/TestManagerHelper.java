/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zenq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.testng.xml.XmlClass;
import org.testng.xml.XmlInclude;
import org.testng.xml.XmlPackage;
import org.testng.xml.XmlSuite;
import org.testng.xml.XmlTest;

/**
 *
 * @author Yatheesh
 */
public class TestManagerHelper {
    String suiteName;
    String linearOrRemote;
    String parallel_testsOrClasses;
    Map localfinalMap=new HashMap();
    Map remoteFinalMap=new HashMap();
    XmlSuite suite ;
    String testName;
    int threadCount;
    String propertiesFile;
    public TestManagerHelper(String suiteName,String linearOrRemote,String parallel_testsOrClasses,Map localfinalMap,Map remoteFinalMap){
         this.propertiesFile=new ProjectPaths().getTempPropertiesFile();
         this.suiteName=suiteName;
         this.linearOrRemote=linearOrRemote;
         this.parallel_testsOrClasses=parallel_testsOrClasses;
         this.localfinalMap=localfinalMap;
         this.remoteFinalMap=remoteFinalMap;
    }
    
     public TestManagerHelper(String suiteName,String linearOrRemote,String parallel_testsOrClasses,Map localfinalMap,Map remoteFinalMap, int threadCount){
         this.propertiesFile=new ProjectPaths().getTempPropertiesFile();
         this.suiteName=suiteName;
         this.linearOrRemote=linearOrRemote;
         this.parallel_testsOrClasses=parallel_testsOrClasses;
         this.localfinalMap=localfinalMap;
         this.remoteFinalMap=remoteFinalMap;
         this.threadCount=threadCount;
    }
    
    public XmlSuite createAndReturnSuite() {
       
    	suite = new XmlSuite();  
        
        //set the list of XmlTests to an XmlSuite         
        suite.setName(suiteName);       
        
        if(linearOrRemote.equals("remote")){
            if(parallel_testsOrClasses.equals("tests"))                
                suite.setParallel(XmlSuite.PARALLEL_TESTS);
            else                
                suite.setParallel(XmlSuite.PARALLEL_CLASSES);            
            //thread-count=\""+remoteFinalMap.size()
            if(threadCount!=0)
                suite.setThreadCount(threadCount);
        }
        else {                
           suite.setParallel(XmlSuite.PARALLEL_TESTS);
           suite.setThreadCount(1);
        }
        
        List<XmlTest> xmlTests=getTests();
        suite.setTests(xmlTests);//setTests(List<XmlTest> tests)
       
        return suite;
    }
    
    public List<XmlTest> getTests(){
        
        Set localOrRemoteEntry=null;
        
        //Add all XmlTests to a List of type "XmlTest"
        List<XmlTest> tests = new ArrayList<XmlTest>();
        
        //iterate the map(local or remote),create tests 
        if(linearOrRemote.equals("remote")){
            if(!remoteFinalMap.isEmpty()){
                
                //the remoteFinalMap contains the following entry
                 
                /* {test12={com.testsuite.email.EmailverificationusingHelperpropertiesfile=windows 8_1::chrome::34, 
                    com.testsuite.dataprovider.DataDrivenTestWithExcel=windows 8_1::chrome::34},
                    test1={com.testsuite.email.EmailFunctionalities=windows vista::chrome::23, 
                    com.testsuite.other.OtherLinks=windows vista::chrome::23}}*/            
                
               localOrRemoteEntry=remoteFinalMap.entrySet();   
            }
            else{
                System.out.println("Remote Final Map is Empty");
            }
        }else{
            
              //the localFinalMap contains the following entry
            
            /*{test1={com.testsuite.dataprovider.DataDrivenTestWithExcel=chrome, 
            com.testsuite.dataprovider.DataDrivenTestWithXML=chrome}}*/
             if(!localfinalMap.isEmpty()){
                localOrRemoteEntry=localfinalMap.entrySet(); 
            }else{
                  System.out.println("Local Final Map is Empty");
             }
        }
        
        Iterator itr=localOrRemoteEntry.iterator(); 
         //each test iteration
         while(itr.hasNext()){
            Map.Entry entry=(Map.Entry)itr.next();
            XmlTest xmlTest=createTest(entry);
            tests.add(xmlTest);
        }
         
        return tests;
    }
   
        
    public XmlTest createTest(Map.Entry entri){
        
        //this "entri" object contain the following kind of entry
        
         /* {test12={com.testsuite.email.EmailverificationusingHelperpropertiesfile=windows 8_1::chrome::34, 
                        com.testsuite.dataprovider.DataDrivenTestWithExcel=windows 8_1::chrome::34}, 
            */
        
         Utilities util=new Utilities();
        
         String os="";
         String browserType="";
         String browser_version="";
         String osVersion="";
         String osName="";
         String methods="";
         
           
        //add all xml classes to a List
        List<XmlClass> clazzes = new ArrayList<XmlClass>();   
        
        //add all xml packages to a List
        List<XmlPackage> packages = new ArrayList<XmlPackage>();   
        
        //List<XmlInclude> includeMethods=new ArrayList<XmlInclude>();       
        //List<String> exludeMethods=new ArrayList<String>();
         
        //set the list of xml classes,parameters and groups to an XmlTest
        XmlTest test = new XmlTest(suite);       
        testName=entri.getKey().toString();
        test.setName(testName);   
      
        
        Map testInfoMap=(HashMap)entri.getValue();
        Set testInfoSet=testInfoMap.entrySet();        
        Iterator iterator=testInfoSet.iterator();        
        int i=0;
        
        while(iterator.hasNext()){
            Map.Entry entry=(Map.Entry)iterator.next();
            //remote
            /* {test12={com.testsuite.email.EmailverificationusingHelperpropertiesfile=windows 8_1::chrome::34, 
                        com.testsuite.dataprovider.DataDrivenTestWithExcel=windows 8_1::chrome::34}, *
            
            //local
            /*{test1={com.testsuite.dataprovider.DataDrivenTestWithExcel=chrome, 
            com.testsuite.dataprovider.DataDrivenTestWithXML=chrome}}*/
            String splittableString=entry.getValue().toString();
            String[] osBrowserVersion=splittableString.split("::");
              
            if(osBrowserVersion.length==3){//for remote
                os=osBrowserVersion[0];
                browserType=osBrowserVersion[1];
                browser_version=osBrowserVersion[2];
            }else{//for local
                browserType=splittableString;
                os=System.getProperty("os.name");
            }
              
            if(os.matches(".*\\d+.*")) {
                osVersion=os.replaceAll("\\D+","");
                osName=os.replace(osVersion, "").trim();
            }else if(!os.matches(".*\\d+.*") && os.contains(" ")){
                String[] verAndName=os.split(" ");
                osName=verAndName[0];
            }
            
            if(entry.getKey().toString().contains(".java")){
                String xmlClass=entry.getKey().toString().replace(".java","");
                String tempPropFile=new ProjectPaths().getTempPropertiesFile();              
                String xmlModClass=xmlClass.replace(".", "|");
                String xmlModClassWithTestNameAppended=testName+"|"+xmlModClass;
                
                methods=util.getValueFromPropFile(tempPropFile, xmlModClassWithTestNameAppended);
                util.removeKey(propertiesFile, xmlModClassWithTestNameAppended);
                
                if(methods!=null){   
                    String[] bothInclExcl=methods.split(" ");
               
                    XmlClass xmlKlass=new XmlClass(xmlClass);
                    List<XmlInclude> includeMethods=new ArrayList<XmlInclude>();
                    List<String> exludeMethods=new ArrayList<String>();
                    for(String meth:bothInclExcl){
                        if(meth.contains("@INCLU@")){ 
                            String method=meth.replace("@INCLU@", "");
                            includeMethods.add(new XmlInclude(method));
                        }
                        if(meth.contains("@EXCLU@")){   
                            String method=meth.replace("@EXCLU@", "");
                            exludeMethods.add(method);
                        }
                    }                 
                    xmlKlass.setIncludedMethods(includeMethods);
                    xmlKlass.setExcludedMethods(exludeMethods);
                    clazzes.add(xmlKlass); //make sure the added "xmlKlass" is available in the class path of the Tool
                    System.out.println("class:"+xmlClass+" successfully added");
                }
                else{                    
                    clazzes.add(new XmlClass(xmlClass));
                }                
             
            }else{
                packages.add(new XmlPackage(entry.getKey().toString()));
            }           
        }
        
        Map parameters=new HashMap();
        parameters.put("browser-Type", browserType);
        if(linearOrRemote.equals("remote"))
            parameters.put("browser-Version", browser_version);
	parameters.put("OS-Name", os);
	parameters.put("Session", "Testing");	
        //parameters.put("URL","");
	test.setParameters(parameters);    
        
        test=setGroups(test);
        
        if(!clazzes.isEmpty())
            test.setClasses(clazzes);
        else
            test.setPackages(packages);
      
        return test;
    }
    
    public XmlTest setGroups(XmlTest test){
        
           Utilities util=new Utilities();
           
           String[] groups=new String[20];
           String groupString=util.getValueFromPropFile(new ProjectPaths().getTempPropertiesFile(),testName); 
           
           if(groupString!=null){
                groups=groupString.split(" "); 
           
                for(String group:groups){
                    if(group.contains("INCLU@")){
                        group=group.replace("INCLU@", "").trim();
                        test.addIncludedGroup(group);
                    }else{
                        group=group.replace("ECLU@", "").trim();
                        test.addExcludedGroup(group);
                    }
                }
                util.removeKey(propertiesFile, testName);
                return test;
           }
           return test;       
    }
}
