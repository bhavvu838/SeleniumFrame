package com.zenq;

import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Yatheesh
 */
public class SystemPropertiesUpdator extends javax.swing.JFrame {
    Map<String,String> existingKeyValue=new HashMap<>();
    Map<String,String> newKeyValue=new HashMap<>();  
    String configFilePath=new ProjectPaths().getSysDotPropertiesFile();
    JFrame parentFrame;
    /**
     * Creates new form SystemPropertiesUpdator
     */
    public SystemPropertiesUpdator(JFrame frame) {  
        parentFrame=frame;       
        initComponents();
        setBackgroundColor();
        setResizable(false);
        //to display the window in the screen center
        setLocationRelativeTo(null);        
        readTheKeysToComboBox();        
        
        //making the parent window stay even when child window(this window) closed
        //set defaultcloseoperation as "dispose" in the window properties otherwise this will not work 
        this.addWindowListener(new WindowAdapter() {           
            @Override
            public void windowClosed(WindowEvent e) {
              System.out.println("window closed");
              parentFrame.setEnabled(true);
              parentFrame.setVisible(true);
            }
        });
    }
    public SystemPropertiesUpdator() {        
        initComponents();
        setBackgroundColor();
        setResizable(false);
        //to display the window in the screen center
        setLocationRelativeTo(null);        
        readTheKeysToComboBox();      
    }
   public void setBackgroundColor(){
        this.getContentPane().setBackground(Color.getHSBColor(180f,180f,180f));      
    }
    public String getGroupsValue(){
         String value="";
        try {
            File file = new File(configFilePath);
            FileInputStream fileInput;
            fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);          
            
            value=(String)properties.get("Groups");
            
            fileInput.close();           
           
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SystemPropertiesUpdator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SystemPropertiesUpdator.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return value;
    }
    
    public void readTheKeysToComboBox(){
        String allSysProperties="";
      try {
            File file = new File(configFilePath);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            fileInput.close();

            Enumeration enuKeys = properties.keys();  
            keyCombo.removeAllItems();
            keyCombo.addItem("Choose Key");
            String keyValuesString="";
            while (enuKeys.hasMoreElements()) {
		String key = (String) enuKeys.nextElement();
               	keyCombo.addItem(key);
                keyValuesString=keyValuesString+ key+" = "+properties.getProperty(key) +"\n";
               
            }
             keysEditor.setText(keyValuesString);
             keysEditor.setEditable(false);
             //sysProperties.setText(allSysProperties);
            } catch (FileNotFoundException e) {
		e.printStackTrace();
            } catch (IOException e) {
		e.printStackTrace();
            }       
    }   

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        keyCombo = new javax.swing.JComboBox();
        valueTextField = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        newKeyTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        newValueTextField = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        updateButton = new javax.swing.JButton();
        saveModifiedKeyValueBtn = new javax.swing.JButton();
        cancelBtn = new javax.swing.JButton();
        saveNewKeyValueBtn = new javax.swing.JButton();
        removeBtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        keysEditor = new javax.swing.JEditorPane();
        jButton1 = new javax.swing.JButton();

        jScrollPane1.setViewportView(jEditorPane1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("SysDotPropertiesFile Updator");

        keyCombo.setToolTipText("");
        keyCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                keyComboActionPerformed(evt);
            }
        });

        jLabel1.setText("Key");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Edit the Sys.properties file");

        jLabel3.setText("Value");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setText("Select key and update value");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setText("create new key and value");

        jLabel6.setText("New key");

        jLabel7.setText("New value");

        updateButton.setText("Update");
        updateButton.setToolTipText("");
        updateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateButtonActionPerformed(evt);
            }
        });

        saveModifiedKeyValueBtn.setText("Save");
        saveModifiedKeyValueBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveModifiedKeyValueBtnActionPerformed(evt);
            }
        });

        cancelBtn.setText("Reset");
        cancelBtn.setToolTipText("resets the above four fields ");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        saveNewKeyValueBtn.setText("Save");
        saveNewKeyValueBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveNewKeyValueBtnActionPerformed(evt);
            }
        });

        removeBtn.setText("Remove");
        removeBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeBtnActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(keysEditor);

        jButton1.setText("close");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jSeparator1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addGap(20, 20, 20)
                                        .addComponent(newKeyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)
                                        .addComponent(jLabel7)
                                        .addGap(20, 20, 20)
                                        .addComponent(newValueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30)
                                        .addComponent(saveNewKeyValueBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(188, 188, 188)
                                        .addComponent(jLabel2))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(238, 238, 238)
                                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(20, 20, 20)
                                                .addComponent(valueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(93, 93, 93))
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(18, 18, 18)
                                                .addComponent(keyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(saveModifiedKeyValueBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(5, 5, 5)))
                                        .addComponent(removeBtn))))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(220, 220, 220)
                                .addComponent(updateButton)
                                .addGap(20, 20, 20)
                                .addComponent(cancelBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(keyCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(saveModifiedKeyValueBtn)
                    .addComponent(removeBtn))
                .addGap(20, 20, 20)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 3, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(newKeyTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7)
                    .addComponent(newValueTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(saveNewKeyValueBtn))
                .addGap(20, 20, 20)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 4, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateButton)
                    .addComponent(cancelBtn)
                    .addComponent(jButton1))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void keyComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_keyComboActionPerformed
         String allSysProperties="";
         String selectedKey=keyCombo.getSelectedItem().toString();
         
         if(selectedKey.equals("Choose Key")){
             readTheKeysToComboBox();
             valueTextField.setText("");
         }
         
      try {
            File file = new File(configFilePath);
            FileInputStream fileInput = new FileInputStream(file);
            Properties properties = new Properties();
            properties.load(fileInput);
            fileInput.close();

            Enumeration enuKeys = properties.keys();
            
            while (enuKeys.hasMoreElements()) {
		String key = (String) enuKeys.nextElement();
                
                if(selectedKey!="ChooseKey"){
                    if(selectedKey.equals(key)){
                        String value = properties.getProperty(key);
                        valueTextField.setText(value);                      
                    }
                }
		
            }            
            } catch (FileNotFoundException e) {
		e.printStackTrace();
            } catch (IOException e) {
		e.printStackTrace();
            }       
    }//GEN-LAST:event_keyComboActionPerformed

    private void saveModifiedKeyValueBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveModifiedKeyValueBtnActionPerformed
        String key=keyCombo.getSelectedItem().toString();
        String value=valueTextField.getText().trim();
        
        if(!key.equals("Choose Key")&&!value.equals("")){
            int ret=JOptionPane.showConfirmDialog(this, "Are you sure, You want to save??");//yes=0, no=1
            if(ret==0){
                existingKeyValue.put(key, value);  
                keyCombo.setSelectedItem("Choose Key");
                valueTextField.setText("");
            }
        }
        else{
            JOptionPane.showMessageDialog(this, "Please choose a key to update");
        }
        System.out.println(existingKeyValue);
    }//GEN-LAST:event_saveModifiedKeyValueBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        cancelBtn.setToolTipText("resets the above four fields");
        existingKeyValue.clear();
        newKeyValue.clear();
        newKeyTextField.setText("");
        newValueTextField.setText("");
        keyCombo.setSelectedIndex(0);
        valueTextField.setText("");
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void saveNewKeyValueBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveNewKeyValueBtnActionPerformed
        String key=newKeyTextField.getText().trim();
        String value=newValueTextField.getText().trim();
        
        if(!key.equals("")&&!value.equals("")){
            int ret=JOptionPane.showConfirmDialog(this, "Are you sure, You want to save??");//yes=0, no=1
            if(ret==0){
                newKeyValue.put(key, value);
                newKeyTextField.setText("");
                newValueTextField.setText("");
            }
        }  
        else if(key.equals("")){
            JOptionPane.showMessageDialog(this, "please enter key");
        }
        else{
            JOptionPane.showMessageDialog(this, "please enter value");
        }
    }//GEN-LAST:event_saveNewKeyValueBtnActionPerformed

    private void updateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateButtonActionPerformed
        
        if(existingKeyValue.size()!=0||newKeyValue.size()!=0 ){
            if(existingKeyValue.size()!=0){
                updateModifiedKeys();
                JOptionPane.showMessageDialog(this, "Modified values successfully saved in:"+configFilePath);
                existingKeyValue.clear();
            }
            if(newKeyValue.size()!=0){
                if(addNewKeys())
                    JOptionPane.showMessageDialog(this, "New keys & values successfully saved in:"+configFilePath);
                newKeyValue.clear();
            }
        }
        else{
            JOptionPane.showMessageDialog(this, "Please choose a key to update or create a new key");
        }
       keyCombo.setSelectedItem("Choose Key");
    }//GEN-LAST:event_updateButtonActionPerformed

    private void removeBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeBtnActionPerformed
                int resp=5;
        try {
            
            if(keyCombo.getSelectedItem()!=null && keyCombo.getSelectedItem()!="Choose Key"){
                resp=JOptionPane.showConfirmDialog(this, "Are you sure you want to delete the key?");
            }
            else{
                JOptionPane.showMessageDialog(this, "Please choose a key to delete");
            }
            
            if(resp==0){
                FileInputStream in = new FileInputStream(configFilePath);
                Properties props = new Properties();
                props.load(in);
                in.close();  
                FileOutputStream fileOut = new FileOutputStream(configFilePath);      
                String selectedKey=keyCombo.getSelectedItem().toString();           
                Object obj=props.remove(selectedKey);           
            
                props.store(fileOut, null);
                fileOut.close();

                if(obj!=null){      
                    JOptionPane.showMessageDialog(this, "Success fully the key "+selectedKey+" removed from the properties file");
                    keyCombo.setSelectedItem("Choose Key");
                    valueTextField.setText("");        
                }            
          }
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
    }//GEN-LAST:event_removeBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton1ActionPerformed
    
    public void updateModifiedKeys(){
        try {
            FileInputStream in = new FileInputStream(configFilePath);
            Properties props = new Properties();
            props.load(in);
            in.close();  
            
            FileOutputStream fileOut = new FileOutputStream(configFilePath);   
            
            Set<String> set=existingKeyValue.keySet();
            Iterator itr=set.iterator();            
            while(itr.hasNext()){
                String key=(String)itr.next();
                String value=existingKeyValue.get(key);
                props.setProperty(key, value);
            }             
            
            props.store(fileOut, null);
            fileOut.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
    }
    public boolean addNewKeys(){
        boolean addedOrNot=false;
        String[] keys=new String[100];
        try {
            FileInputStream in = new FileInputStream(configFilePath);
            Properties props = new Properties();
            props.load(in);
            
            Set s=props.keySet();
            Iterator itrr=s.iterator();
            int i=0;
            while(itrr.hasNext()){
                String key=(String)itrr.next();               
                keys[i]=key;
                i++;
            }
            in.close();  
            
            FileOutputStream fileOut = new FileOutputStream(configFilePath);   
            
            Set<String> set=newKeyValue.keySet();
            Iterator itr=set.iterator();            
            while(itr.hasNext()){
                String key=(String)itr.next();
                String value=newKeyValue.get(key);                
                 for(int k=0;k<keys.length;k++){
                     if(keys[k]!=null){
                        String keyfromprop=keys[k];
                        if(keyfromprop.equalsIgnoreCase(key)){
                             addedOrNot=true;      
                        }                      
                     }
                  }
                 if(addedOrNot)
                     JOptionPane.showMessageDialog(this, "duplicate key:"+key+" will not be added");
                 else{
                     System.out.println("Key is:"+key+" value is:"+value);
                    // props.setProperty(key, value);
                     props.put(key, value);
                    
                 }
                
            }             
            
            props.store(fileOut, null);
            fileOut.close();
	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
        return !addedOrNot;
    }
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton jButton1;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JComboBox keyCombo;
    private javax.swing.JEditorPane keysEditor;
    private javax.swing.JTextField newKeyTextField;
    private javax.swing.JTextField newValueTextField;
    private javax.swing.JButton removeBtn;
    private javax.swing.JButton saveModifiedKeyValueBtn;
    private javax.swing.JButton saveNewKeyValueBtn;
    private javax.swing.JButton updateButton;
    private javax.swing.JTextField valueTextField;
    // End of variables declaration//GEN-END:variables
}
