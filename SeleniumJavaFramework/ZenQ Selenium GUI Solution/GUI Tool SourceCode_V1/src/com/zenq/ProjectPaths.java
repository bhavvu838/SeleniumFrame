/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.zenq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Yatheesh
 */
public class ProjectPaths {
    
   String projectDir;
   String configFilePath;
   String tempPropertiesFile;
   String testSuitesPath;
   String buildDotXml;
   String antBuildBatFile;
   String sysDotPropertiesFile;
   String testSrc;
   String jarPath;
   
    public ProjectPaths() {
       
       try {
           BufferedReader br=new BufferedReader(new FileReader(System.getProperty("java.io.tmpdir")+File.separator+"temp.txt"));
           projectDir=br.readLine();           
       }  catch (FileNotFoundException ex) {
           Logger.getLogger(ProjectPaths.class.getName()).log(Level.SEVERE, null, ex);
       }  catch (IOException ex) {
           Logger.getLogger(ProjectPaths.class.getName()).log(Level.SEVERE, null, ex);
       } 
        
       configFilePath=projectDir+File.separator+"ConfigFiles";
       tempPropertiesFile=configFilePath+File.separator+"temp.properties";
       sysDotPropertiesFile=configFilePath+File.separator+"Sys.properties";
       testSuitesPath=projectDir+File.separator+"TestNGSuites";
       buildDotXml=projectDir+File.separator+"Build.xml";
       antBuildBatFile=projectDir+File.separator+"Ant_Build.bat";
       testSrc=projectDir+File.separator+"test-src";
       Path workingDirectory=Paths.get(".").toAbsolutePath();
       jarPath=workingDirectory.toString().replace("\\.", "")+"\\lib\\SeleniumProjectBuild.jar";
       
    }
    
    public String getJarPath(){
        return jarPath;
    }
    
    public String getTestSrc(){
        return testSrc;
    }
    
    public String getTempPropertiesFile(){
       try {           
           File temp=new File(tempPropertiesFile);
           if(!temp.exists())
               new File(tempPropertiesFile).createNewFile();  
           
           return tempPropertiesFile;
       } catch (IOException ex) {
           Logger.getLogger(ProjectPaths.class.getName()).log(Level.SEVERE, null, ex);
       }
        return null;
    }
    
    public String getSysDotPropertiesFile(){
        return sysDotPropertiesFile;
    }
    
    public String getAntBuildBatFile(){
        return antBuildBatFile;
    }
    
    public String getProjectDir(){
        return projectDir;
    }
    
    public String getConfigFilePath(){
        return configFilePath;
    }
    
    public String getTestSuitesPath(){
        return testSuitesPath;
    }
    
    public String getBuildDotXml(){
        return buildDotXml;
    }
}
