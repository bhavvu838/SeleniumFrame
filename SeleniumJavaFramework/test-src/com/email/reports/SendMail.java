package com.email.reports;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.datamanager.ConfigManager;
import com.passworddecoder.PasswordDecorder;

public class SendMail {
	
	static ConfigManager config = new ConfigManager("Sys");
	static Properties mailServerProperties;
	static Session getMailSession;
	static MimeMessage generateMailMessage;
	static BodyPart messageBodyPart;
	static String sRecipientMail = config.getProperty("TestData.RecipientMail");
	static String[] sRecipientList = sRecipientMail.split(",");
	static String sCCMail = config.getProperty("TestData.CCMail");;
	static String[] sCCList = sCCMail.split(",");
	static String sSenderMail = config.getProperty("TestData.SenderMail");
	static String sSenderMailPassword = config.getProperty("TestData.SenderPassword");
	static Transport transport;
	static MimeMultipart mimemultipart;
	static MimeBodyPart imagePart;
	public static String fileSeparator = System.getProperty("file.separator");
	static String zFilePath = System.getProperty("user.dir") +fileSeparator+"target"+fileSeparator+"TestResults.zip";
	static String sImagePath = System.getProperty("user.dir") +fileSeparator+"Automation Reports"+fileSeparator+"LatestResults"+fileSeparator+"html"+fileSeparator+"Reports.png";
	static GenerateZipAndImageFiles gen;

public static void main(String arg[]) throws IOException {
	if(config.getProperty("SendEmail").equalsIgnoreCase("true")){
		try {
			gen = new GenerateZipAndImageFiles();
			gen.addZipAndImage();
			Map<String, String> inlineImages = new HashMap<String, String>();
			inlineImages.put("report", zFilePath);

			System.out.println("Setup Mail Server Properties..");
			mailServerProperties = System.getProperties();
			mailServerProperties.put("mail.smtp.port", "465");
			mailServerProperties.put("mail.smtp.auth", "true");
			mailServerProperties.put("mail.smtp.starttls.enable", "true");
			mailServerProperties.put("mail.smtp.socketFactory.port", "465");
			mailServerProperties.put("mail.smtp.socketFactory.class",
					"javax.net.ssl.SSLSocketFactory");
			System.out
					.println("Mail Server Properties have been setup successfully..");
			System.out.println("Get Mail Session..");
			getMailSession = Session.getDefaultInstance(mailServerProperties,
					null);
			generateMailMessage = new MimeMessage(getMailSession);
			mimemultipart = new MimeMultipart("related");
			
			for(int i=0;i<(sRecipientList.length);i++){
			generateMailMessage.addRecipient(Message.RecipientType.TO,
					new InternetAddress(sRecipientList[i]));
			}
			if(!(((sCCList.length)==1)&&((sCCList[0].trim()).length()==0))){
			for(int j=0;j<(sCCList.length);j++){
			generateMailMessage.addRecipient(Message.RecipientType.CC,
					new InternetAddress(sCCList[j]));
			}
			}
			generateMailMessage.setSubject(config.getProperty("TestData.MailSubject"));
			messageBodyPart = new MimeBodyPart();
			messageBodyPart.setContent("<img src=\"cid:image\">", "text/html");
			mimemultipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			DataSource fds = new FileDataSource(sImagePath);
			messageBodyPart.setDataHandler(new DataHandler(fds));
			messageBodyPart.setHeader("Content-ID", "<image>");
			mimemultipart.addBodyPart(messageBodyPart);
			generateMailMessage.setContent(mimemultipart);

			if (inlineImages != null && inlineImages.size() > 0) {
				Set<String> setImageID = inlineImages.keySet();

				for (String contentId : setImageID) {
					imagePart = new MimeBodyPart();
					imagePart.setHeader("Content-ID", "<" + contentId + ">");
					imagePart.setDisposition(MimeBodyPart.INLINE);
					String imageFilePath = inlineImages.get(contentId);

					imagePart.attachFile(imageFilePath);

					mimemultipart.addBodyPart(imagePart);
				}
			}

			generateMailMessage.setContent(mimemultipart);

			System.out.println("Mail Session has been created successfully..");
			transport = getMailSession.getTransport("smtp");

			// Enter your correct gmail UserID and Password
			// if you have 2FA enabled then provide App Specific Password
			transport.connect("smtp.gmail.com", sSenderMail,
					PasswordDecorder.passwordDecrypt(sSenderMailPassword));
			transport.sendMessage(generateMailMessage,
					generateMailMessage.getAllRecipients());
			transport.close();
			System.out.println("Your Reports has been sent to:- "
					+ sRecipientMail + " from " + sSenderMail);

		} catch (IOException ex) {
			ex.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
}
