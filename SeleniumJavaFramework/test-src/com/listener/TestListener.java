package com.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ISuite;
import org.testng.ISuiteListener;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;
import org.testng.annotations.Optional;

import ru.yandex.qatools.allure.annotations.Attachment;

import com.datamanager.ConfigManager;
import com.gurock.testrail.TestRailResultUpdator;
import com.testng.Assert;
import com.utilities.ReportSetup;
import com.utilities.ScreenCapture;
import com.utilities.UtilityMethods;

public class TestListener extends TestListenerAdapter implements ISuiteListener {

	private static char cQuote = '"';
	ConfigManager sys = new ConfigManager();
	ConfigManager depend = new ConfigManager("TestDependency");
	private static String fileSeperator = System.getProperty("file.separator");
	Logger log = Logger.getLogger("TestListener");
	private final String PASSED = "PASSED";
	private final String FAILED = "FAILED";
	private final String SKIPPED = "SKIPPED";
	private final String DASHBOARD_DIR = System.getProperty("user.dir") + fileSeperator + "Dashboard";
	TestRailResultUpdator testRailResultUpdator = new TestRailResultUpdator();
	private static boolean milestoneCreated = false;

	/**
	 * This method will be called if a test case is failed. Purpose - For
	 * attaching captured screenshots and videos in ReportNG report
	 */
	public void onTestFailure(ITestResult result) {
		depend.writeProperty(result.getName(), "Fail");

		log.error("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
		log.error("ERROR ----------" + result.getName() + " has failed-----------------");
		log.error("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\n");
		ITestContext context = result.getTestContext();
		WebDriver driver = (WebDriver) context.getAttribute("driver");
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		Reporter.setCurrentTestResult(result);
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 5);
		String screenshotName = ScreenCapture.saveScreenShot(driver);
		String imagepath = ".." + fileSeperator + "Screenshots" + fileSeperator + screenshotName;
		createAttachment(driver);
		Reporter.log("<a href=" + cQuote + imagepath + cQuote + ">" + " <img src=" + cQuote + imagepath + cQuote
				+ " height=48 width=48 ></a>");
		ScreenCapture.stopVideoCapture(result.getName());
		updateRuntimeReport(result, Thread.currentThread().getId(), FAILED);
		UtilityMethods.verifyPopUp();
		String sValue = new ConfigManager().getProperty("VideoCapture");
		String sModeOfExecution = new ConfigManager().getProperty("ModeOfExecution");
		if (sValue.equalsIgnoreCase("true") && sModeOfExecution.equalsIgnoreCase("linear")) {
			String sVideoPath = null;
			sVideoPath = testCaseVideoRecordingLink(result.getName());
			Reporter.log("<a href=" + cQuote + sVideoPath + cQuote + " style=" + cQuote
					+ "text-decoration: none; color: white;" + cQuote
					+ "><div class = cbutton>Download Video</div></a>");
			Reporter.log("<font color='Blue' face='verdana' size='2'><b>" + Assert.doAssert() + "</b></font>");
			// Reporter.log("<a color='Blue' face='verdana'
			// size='2'><b>"+Assert.doAssert()+"</b></a>");
		}
		Reporter.setCurrentTestResult(null);

		String screenshot = System.getProperty("user.dir") + fileSeperator + "Automation Reports" + fileSeperator
							+ "LatestResults" + fileSeperator + "Screenshots" + fileSeperator + screenshotName;
//		captureTestStatusIntoDB(result, "FAILED", result.getThrowable().getMessage(), screenshot);

	}

	/**
	 * Method to capture screenshot for allure reports
	 * 
	 * @param driver
	 *            , need to pass the driver object
	 * @return , returns the captured image file in the form of bytes
	 */
	@Attachment(value = "Screenshot", type = "image/png")
	private byte[] createAttachment(WebDriver driver) {
		try {
			return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
		} catch (Exception e) {
			log.error(
					"An exception occured while saving screenshot of current browser window from createAttachment method.."
							+ e.getCause());
			return null;
		}
	}

	public void test() {

	}

	/**
	 * This method will be called if a test case is skipped.
	 * 
	 */
	public void onTestSkipped(ITestResult result) {
		log.warn("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		log.warn("WARN ------------" + result.getName() + " has skipped-----------------");
		log.warn("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		updateRuntimeReport(result, Thread.currentThread().getId(), SKIPPED);
		depend.writeProperty(result.getName(), "Skip");

		// ************* comment below code if you are using TestNG dependency
		// methods
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 2);
		Reporter.setCurrentTestResult(result);
		ScreenCapture.stopVideoCapture(result.getName());
		UtilityMethods.verifyPopUp();
		Reporter.setCurrentTestResult(null);

	//	captureTestStatusIntoDB(result, "SKIPPED", result.getThrowable().getMessage(), "");
	}

	/**
	 * This method will be called if a test case is passed. Purpose - For
	 * attaching captured videos in ReportNG report
	 */
	public void onTestSuccess(ITestResult result) {
		depend.writeProperty(result.getName(), "Pass");
		System.setProperty("org.uncommons.reportng.escape-output", "false");
		log.info("###############################################################");
		log.info("SUCCESS ---------" + result.getName() + " has passed-----------------");
		log.info("###############################################################");
		String id = getCaseId(result.getName());
		updateToTestRail(id, result.getName(), 1);
		Reporter.setCurrentTestResult(result);
		ScreenCapture.stopVideoCapture(result.getName());
		UtilityMethods.verifyPopUp();
		updateRuntimeReport(result, Thread.currentThread().getId(), PASSED);
		String sValue = new ConfigManager().getProperty("VideoCapture");
		String sModeOfExecution = new ConfigManager().getProperty("ModeOfExecution");
		if (sValue.equalsIgnoreCase("true") && sModeOfExecution.equalsIgnoreCase("linear")) {
			String sVideoPath = testCaseVideoRecordingLink(result.getName());
			Reporter.log("<a href=" + cQuote + sVideoPath + cQuote + " style=" + cQuote
					+ "text-decoration: none; color: white;" + cQuote
					+ "><div class = cbutton>Download Video</div></a>");
		}
		Reporter.setCurrentTestResult(null);

	//	captureTestStatusIntoDB(result, "PASSED", "", "");
	}

	/**
	 * This method will be called before a test case is executed. Purpose - For
	 * starting video capture and launching balloon popup in ReportNG report
	 */
	public void onTestStart(ITestResult result) {
		log.infoLevel("<h2>**************CURRENTLY RUNNING TEST************ " + result.getName() + "</h2>");
		createFile(Thread.currentThread().getId());
		ScreenCapture.startVideoCapture();
		UtilityMethods.currentRunningTestCaseBalloonPopUp(result.getName());
	}

	public void onStart(ITestContext context) {

	}

	public void onFinish(ITestContext context) {
		Iterator<ITestResult> failedTestCases = context.getFailedTests().getAllResults().iterator();
		while (failedTestCases.hasNext()) {
			ITestResult failedTestCase = failedTestCases.next();
			ITestNGMethod method = failedTestCase.getMethod();
			if (context.getFailedTests().getResults(method).size() > 1) {
				if (sys.getProperty("KeepFailedResult").equalsIgnoreCase("false")) {
					// log.info("failed test case remove as dup:" +
					// failedTestCase.getTestClass().toString());
					failedTestCases.remove();
				}
			} else {
				if (context.getPassedTests().getResults(method).size() > 0) {
					if (sys.getProperty("KeepFailedResult").equalsIgnoreCase("false")) {
						// log.info("failed test case remove as pass retry:" +
						// failedTestCase.getTestClass().toString());
						failedTestCases.remove();
					}
				}
			}
		}
	}

	/**
	 * 
	 * To identify the latest captured screenshot
	 *
	 * @return
	 */
	public String capturedScreenShot() {

		File mediaFolder = new File(ReportSetup.getImagesPath());
		File[] files = mediaFolder.listFiles();
		Arrays.sort(files, new Comparator<Object>() {
			public int compare(Object o1, Object o2) {
				// return new Long(((File)o1).lastModified()).compareTo(new
				// Long(((File)o2).lastModified())); // for ascending order
				return -1 * (new Long(((File) o1).lastModified()).compareTo(new Long(((File) o2).lastModified()))); // for
																													// descending
																													// order
			}
		});
		return files[0].getName();
	}

	/**
	 * 
	 * This method is used to rename the captured video with test case name
	 *
	 * @param tname
	 *            , Need to pass the test case name @return, Returns the
	 *            captured video path name
	 */
	public String testCaseVideoRecordingLink(String tname) {
		String sVideoPath = ".." + fileSeperator + "Videos" + fileSeperator + tname + "(1).avi";
		if (new File(ReportSetup.getVideosPath() + fileSeperator + tname + "(1).avi").exists()) {
			return sVideoPath;
		} else {
			String sVideoPath2 = sVideoPath.substring(0, sVideoPath.length() - 7) + ".avi";
			return sVideoPath2;
		}
	}

	private void updateRuntimeReport(ITestResult result, long theadId, String strStatus) {
		int count = 0;
		int dpmethodcount = 0;
		StringBuffer param = new StringBuffer();
		String p = "";
		param.append("(");
		ConfigManager rs = new ConfigManager(getFilePath(theadId));
		Object[] params = result.getParameters();
		for (int i = 0; i < params.length; i++) {
			param.append(params[i] + ",");
		}
		// param.append(")");
		if (param.length() > 2) {
			count++;
			p = param.substring(0, param.length() - 2) + ")".toString();
		} else {
			count = 0;
		}
		rs.writeToDashboard(result.getMethod().getRealClass().getName() + "-" + result.getName() + p, strStatus);

		rs = null;
	}

	@Override
	public void onFinish(ISuite arg0) {
		// TODO Auto-generated method stub
		ConfigManager suiteFile = new ConfigManager(getFilePath(Thread.currentThread().getId()));
		suiteFile.writeToDashboard("SUITE_STATUS", "COMPLETED");

	}

	@Override
	public void onStart(ISuite iSuite) {
		for (File file : UtilityMethods.fileList(DASHBOARD_DIR, ".properties")) {
			file.delete();
		}
		File dir = new File(DASHBOARD_DIR);
		if (!dir.exists()) {
			dir.mkdir();
		}
		createFile(Thread.currentThread().getId());
		ConfigManager suiteFile = new ConfigManager(getFilePath(Thread.currentThread().getId()));
		suiteFile.writeToDashboard("TOTAL_TEST_SCRIPTS", String.valueOf(iSuite.getAllMethods().size()));
		suiteFile.writeToDashboard("SUITE_NAME", iSuite.getName());
		if (sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")) {
			if (!milestoneCreated) {
				testRailResultUpdator.createMilestoneAndAddRuns();
				milestoneCreated = true;
			}
		}
	}

	private String getFilePath(long ld) {
		return DASHBOARD_DIR + fileSeperator + ld;
	}

	private void createFile(long ld) {
		File file = new File(getFilePath(ld) + ".properties");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				log.error("Can not create file in specified path : " + file.getAbsolutePath()
						+ UtilityMethods.getStackTrace());
				Assert.fail("Can not create file in specified path : " + file.getAbsolutePath() + e.getMessage());
			}
			file = null;
		}
	}

	/**
	 * 
	 * This method is used to rename the captured video with test case name
	 *
	 * @param tname
	 *            , Need to pass the test case name
	 * @param sessionId,
	 *            Need to pass the remote webdriver session id
	 * @return tcNameVideo, Returns the captured video path name
	 */
	public String testCaseGridVideoRecordingLink(String tname, String sessionId) {
		String sessionVideo = ReportSetup.getVideosPath() + fileSeperator + sessionId + ".mp4";
		File sessionVideoFile = new File(sessionVideo);
		String tcNameVideo = ReportSetup.getVideosPath() + fileSeperator + tname + ".mp4";
		File tcNameFile = new File(tcNameVideo);
		if (sessionVideoFile.renameTo(tcNameFile)) {
			log.info("renamed");
		} else {
			log.error("Error - File is not renamed");
		}
		if (tcNameFile.exists()) {
			return tcNameVideo;
		} else {
			log.error("Error - File is not found");
		}
		return tcNameVideo;
	}

	/**
	 * This method is used to update the results to testrail
	 * 
	 * @param id
	 *            - test case id
	 * @param testName
	 *            - name of the test method
	 * @param testStatus
	 *            - test method status
	 */
	private void updateToTestRail(String id, String testName, int testStatus) {
		if (sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")) {
			if (UtilityMethods.isNumeric(id))
				testRailResultUpdator.addResultToTestRail(testStatus, Integer.parseInt(id));
		}
	}

	/**
	 * This method is used to get the test case id
	 * 
	 * @param strTestName
	 *            - name of the test method
	 * @return test case id if test method contains id, otherwise return null
	 */
	private String getCaseId(String strTestName) {
		if (sys.getProperty("UpdateResultsToTestRail").equalsIgnoreCase("true")) {
			String[] arr = strTestName.split("_");
			return arr[arr.length - 1];
		} else
			return null;
	}

	public void captureTestStatusIntoDB(ITestResult result, String testResult, String errorMessage, String img) {

		// Connection String related values fetched from sys.properties file
		String JDBC_DRIVER = sys.getProperty("JDBC_DRIVER");
		String DB_URL = sys.getProperty("DB_URL");
		String USERNAME = sys.getProperty("USERNAME");
		String PASSWORD = sys.getProperty("PASSWORD");
		String TABLENAME = sys.getProperty("TABLENAME");
		
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		try {
			// Registering JDBC driver
			Class.forName(JDBC_DRIVER);

			// Opening a connection
			log.info("Connecting to database...");
			conn = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

			// Creating statement
			log.info("Creating statement...");
			stmt = conn.createStatement();

			String toolName = "Selenium";
			String projectName = "SeleniumJavaFramework";
			String testName = result.getName();

			long l1 = result.getStartMillis();
			long l2 = result.getEndMillis();
			Date date1 = new Date(l1);
			String timeStamp = date1.toString();

			long time = l2 - l1;
			String executionTime = String.valueOf(time / 1000);

			String desktoposname = System.getProperty("os.name");

			ITestContext context = result.getTestContext();
			WebDriver wd = (WebDriver) context.getAttribute("driverWithoutWebListener");
			Capabilities cap = ((RemoteWebDriver) wd).getCapabilities();
			String browserName = cap.getBrowserName();
			String browserVersion = cap.getVersion();

			String local_remote = sys.getProperty("ModeOfExecution");

			String insertQuery = "INSERT INTO "+TABLENAME+" (toolname, projectname, testname, result, errormessage, timestamp, screenshot,mobiledeviceosname,mobiledeviceosversion,mobiledevicemodel,desktoposname,desktopbrowsername,desktopbrowserversion,executiontime,localorremote) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			PreparedStatement statement = conn.prepareStatement(insertQuery);
			statement.setString(1, toolName);
			statement.setString(2, projectName);
			statement.setString(3, testName);
			statement.setString(4, testResult);
			statement.setString(5, errorMessage);
			statement.setString(6, timeStamp);
			if (testResult.equalsIgnoreCase("FAILED")) {
				File file = new File(img);
				FileInputStream fis = new FileInputStream(file);
				int len = (int) file.length();
				statement.setBinaryStream(7, fis, len);
			} else {
				statement.setBinaryStream(7, null);
			}

			statement.setString(8, "");
			statement.setString(9, "");
			statement.setString(10, "");
			statement.setString(11, desktoposname);
			statement.setString(12, browserName);
			statement.setString(13, browserVersion);
			statement.setString(14, executionTime);
			statement.setString(15, local_remote);

			int rowsInserted = statement.executeUpdate();
			if (rowsInserted > 0) {
				log.info("Current test's status was inserted into DB table successfully!");
			}else{
				log.info("Unable to insert current test's status into DB!");
			}

			rs.close();
			stmt.close();
			conn.close();
		} catch (SQLException se) {
			// Handle errors for JDBC
			se.printStackTrace();
			log.info(" SQLException -- " + se.getMessage());
		} catch (Exception e) {
			// Handle errors for Class.forName
			// e.printStackTrace();
			log.error("Exception occurred is -- " + e.getMessage());
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					stmt.close();
			} catch (SQLException se2) {
				log.info("Finally Statement cannot be closed due to SQLException -- " + se2.getMessage());
			} // nothing we can do
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
				log.info("Finally Connection cannot be closed due to SQLException -- " + se.getMessage());
			} // end finally try
		} // end try
		log.info("DB part is done");

	}

}
