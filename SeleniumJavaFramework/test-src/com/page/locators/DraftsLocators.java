package com.page.locators;

import org.openqa.selenium.By;

public interface DraftsLocators 
{
	public static By DRAFT_PAGE = By.xpath(".//font[text()='Draft']");
	public static By SUBJECTLINE = By.xpath(".//span[text()='%s']");
	public static By DRAFT_MAIL_BOX_COUNT_AND = By.xpath("//div[text() = 'Drafts']/span");
	public static By DRAFT_PAGE_AND = By.xpath("//div[text() = 'Drafts']");
}
