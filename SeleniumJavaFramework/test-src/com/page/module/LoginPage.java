package com.page.module;

import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Step;

import com.page.locators.LogInLocators;
import com.selenium.SafeActions;
import com.testng.Assert;


public class LoginPage extends SafeActions implements LogInLocators
{
	private WebDriver driver;
	
	
	//Constructor to define/call methods	 
	public LoginPage(WebDriver driver)
	{ 
		super(driver);
		this.driver = driver;
    } 
	
	/**
	 * Purpose- To verify whether gmail login page is being displayed or not
	 * @throws Exception
	 */
	@Step("Verifying login page")
	public void verifyLoginPage() 
	{
		boolean bIsSignInButtonExists = isElementPresent(SIGNIN_BTN, SHORTWAIT);
		boolean bIsNextButtonExists = isElementPresent(NEXT_BTN, SHORTWAIT);
		Assert.assertTrue(bIsSignInButtonExists||bIsNextButtonExists,"Sign In/Next button is not being displayed on 'Login' page");
	}
	
	/**
	 * Purpose- To enter login credentilas i.e., username and password 
	 * @param sUsername- we pass username of gmail application
	 * @param sPassword- we pass passowrd of gmail application
	 * @throws Exception
	 */
	@Step("Entering login credentials")
	public void enterLoginCredentials(String sUsername, String sPassword) 
	{
		if(!safeGetAttribute(EMAIL_FIELD, "value", "'Email' input field in 'Login' page",MEDIUMWAIT).equalsIgnoreCase(sUsername))
		{
			safeType(EMAIL_FIELD, sUsername,"'Email' field in 'Login' page", SHORTWAIT);	
			safeClick(NEXT_BTN,"'Next' button in 'Login' page",SHORTWAIT);
		}
		
		safeTypePassword(PASSWORD_FIELD, sPassword,"'Password' input field in 'Login' page", SHORTWAIT);
	}
	
	/**
	 * Purpose- To click on Sign In button
	 * @throws Exception
	 */
	@Step("Clicking on SignIn button")
	public HomePage clickSignInButton() 
	{
		safeClick(SIGNIN_BTN, "'Sign In' button in 'Login' page ",SHORTWAIT);
		waitForSecs(5);
		waitForPageToLoad(VERYLONGWAIT);
		return new HomePage(driver);
	}
}
