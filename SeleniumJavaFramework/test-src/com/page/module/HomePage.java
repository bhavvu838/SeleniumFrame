package com.page.module;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Parameters;

import ru.yandex.qatools.allure.annotations.Step;

import com.datamanager.ConfigManager;
import com.page.locators.HomeLocators;
import com.selenium.Dynamic;
import com.selenium.SafeActions;
//import org.testng.Assert;
import com.testng.Assert;


public class HomePage extends SafeActions implements HomeLocators
{
	private WebDriver driver;
	ConfigManager con;

	//Constructor to define/call methods	 
	public HomePage(WebDriver driver) 
	{		
		super(driver);
		this.driver = driver;
		con = new ConfigManager();
	} 

	/**
	 * Purpose- To verify whether login is successful or not
	 * @throws Exception
	 */
	@Step("Verifying Gmail user name after logging in")
	public void verifyLogin(String sMailId)
	{
		if(!con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
		boolean bIsGmailUserNameExists = isElementPresent(Dynamic.getNewLocator(GMAIL_USER_BTN,sMailId), LONGWAIT);
		Assert.assertTrue(bIsGmailUserNameExists,"Username is not being displayed after clicking on SignIn button");
		}
	}

	/**
	 * Purpose- To click on compose button
	 * @throws Exception
	 */
	@Step("Clicking on Compose button")
	public ComposePage clickComposeButton() 
	{		

		if(con.getProperty("BrowserName").contains("mobile")){
			waitForSecs(4);
			if(isElementPresent(USINGGMAIL_TEXT,LONGWAIT))
				refresh();

			safeClick(COMPOSE_BTN_AND,"'Compose' button in 'Home' page" ,SHORTWAIT);
		}
		else{
			safeClick(COMPOSE_BTN,"'Compose' button in 'Home' page" ,SHORTWAIT);	
		}
		return new ComposePage(driver);
	}

	/**
	 * Purpose- To navigate to Drafts page
	 * @throws Exception
	 */
	//@Step("Clicking on Darfts Link"

	public DraftsPage clickDraftsLink() 
	{
		if(con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			safeClick(MENU_ICON_AND, "menu icon in mobile", MEDIUMWAIT);
			safeClick(DRAFT_BOX_AND, "draft mails tab", MEDIUMWAIT);
		}
		else{
			safeClick(DRAFTS_LINK,"'Drafts' link in 'Home' page",SHORTWAIT);
		}
		return new DraftsPage(driver);
	}

	/**
	 * Purpose- To navigate to trash page
	 * @throws Exception
	 */
	@Step("Navigating to trash page")
	public TrashPage navigateToTrashPage()
	{
		safeClick(MORE_LNK,"'More' link in Home page",SHORTWAIT);
		safeClick(TRASH_LNK,"'Trash' link in Home page",SHORTWAIT);
		return new TrashPage(driver);
	}

	/**
	 * Purpose- To navigate to contacts page
	 * @throws Exception
	 */
	@Step("Navigating to contacts page")
	public ContactsPage navigateToContactsPage() 
	{
		safeClick(GMAIL_DRP,"'Gmail'link in 'Home' page", MEDIUMWAIT);
		safeClick(CONTACTS, "'Contacts' link in 'Home' page" ,LONGWAIT);
		waitForPageToLoad();
		return new ContactsPage(driver);
	}

	/**
	 * Purpose- To navigate to Inbox
	 * @throws Exception
	 */
	@Step("Clicking in Inbox link")
	public InboxPage clickInboxLink() 
	{
		if(con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			refresh();
			safeClick(MENU_ICON_AND, "menu icon in mobile", MEDIUMWAIT);
			safeClick(PRIMARY_INBOX_AND, "primary mails tab in inbox", MEDIUMWAIT);
		}
		else{
			safeClick(INBOX_LNK, "'Inbox' link in 'Home' page",SHORTWAIT);
		}
		return new InboxPage(driver);
	}

	/**
	 * Purpose- To verify Details footer link
	 * @throws Exception
	 */
	@Step("Verifying Details footer link")
	public void verifyDetailsFooterLink() 
	{
		safeClick(DETAILS_LNK, "'Details' link in 'Home' Page",SHORTWAIT);
		waitForPageToLoad();
		switchToWindow(1);
		boolean bDetailspage = isElementPresent(ACTIVITYACCOUNT_LNK, SHORTWAIT);
		Assert.assertTrue(bDetailspage,"On clicking Footer link- Details, Account details page is not being displayed");
		driver.close();
		switchToWindow(0);

	}

	/**
	 * Purpose to verify terms and privacy footer link
	 */
	@Step("Verifying Terms and Privacy links")
	public void verifyTermsAndPrivacyLink() 
	{
		safeClick(PRIVACY_LNK,"'Terms & Privacy' footer link in 'Home' page",MEDIUMWAIT);	
		waitForPageToLoad();
		switchToWindow(1);
		Assert.assertTrue(isAllLinksOnTermsAndPrivacyPagedisplayed(),"Terms and Privacy page is not being displayed successfully on clicking Terms & Privacy footer link");
		driver.close();
		switchToWindow(0);

	}

	/**
	 * Purpose- to verify the links on Terms and privacy page
	 * @return
	 */
	public boolean isAllLinksOnTermsAndPrivacyPagedisplayed() 
	{
		boolean bFaq = isElementPresent(FAQ_LNK, MEDIUMWAIT);
		boolean bPrivacyPolicy = isElementPresent(PRIVACYPOLICY_LNK,MEDIUMWAIT);
		boolean bOverview = isElementPresent(OVERVIEW_LNK, MEDIUMWAIT);
		boolean bTermsofService = isElementPresent(TERMSOFSERVICE_LNK,MEDIUMWAIT);
		boolean bAllLinks = (bFaq && bPrivacyPolicy && bOverview && bTermsofService);
		return bAllLinks;
	}

	/**
	 * Purpose- click on Username and click on logout button
	 */
	@Step("Clicking on Logout link")
	public void clickLogOutButton(String sMailId) 
	{	

		if(con.getProperty("BrowserName").contains("mobile")){

			refresh();
			if(isElementPresent(MENU_ICON_AND)){
				safeClick(MENU_ICON_AND, "menu icon in mobile", MEDIUMWAIT);
				if(isElementPresent(Dynamic.getNewLocator(GMAIL_USER_TOP, sMailId))){
					safeClick(Dynamic.getNewLocator(GMAIL_USER_TOP, sMailId), "gmail user name at top on menu", MEDIUMWAIT);
					safeClick(GMAILL_SIGNOUT_BTN_AND, "signout button", MEDIUMWAIT);
				}
			}
		}
		else{
			safeJavaScriptClick(Dynamic.getNewLocator(GMAIL_USER_BTN,sMailId), "'Gmail User' button",MEDIUMWAIT);
			safeJavaScriptClick(GMAIL_SIGNOUT_BTN,"'Gmail Sign Out' button in 'Home' page",MEDIUMWAIT);
			waitForPageToLoad();
		}
	}

	/**
	 * Purpose- To verify whether logout is successful or not
	 * @throws Exception
	 */
	@Step("Verifying logout page")
	public LoginPage verifyLogOut()
	{

		waitForPageToLoad(VERYLONGWAIT);
		if(!con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			boolean bLogout = isElementPresent(GMAIL_SIGNIN_BTN, VERYLONGWAIT);		
			Assert.assertTrue(bLogout,"Sign In button is not being displayed on Login page after clicking on SignOut button");
		}
		return new LoginPage(driver);
	}
}
