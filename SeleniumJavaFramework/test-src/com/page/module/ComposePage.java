package com.page.module;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Step;
import com.datamanager.ConfigManager;
import com.page.data.SendMailData;
import com.page.locators.ComposeLocators;
import com.selenium.SafeActions;
import com.selenium.Sync;
//import org.testng.Assert;
import com.testng.Assert;


public class ComposePage extends SafeActions implements ComposeLocators
{
	private WebDriver driver;
	SendMailData filldata;
	ConfigManager app;
	Sync sync;
	ConfigManager con;
	
	//Constructor to define/call methods	 
	public ComposePage(WebDriver driver)
	{		
		super(driver);
		this.driver = driver;
		app=new ConfigManager("App");
		con = new ConfigManager();
    } 

	
	/**
	 * Purpose- To verify whether compose a new mail page is being displayed or not
	 * @throws Exception
	 */
	@Step("Verifying compose page")
	public void verifyComposePage()
	{
		waitUntilElementDisappears(LOADING,"'Loading....' Element in 'Compose' page");
		boolean bIsSendButtonExists = isElementPresent(SEND_BTN, MEDIUMWAIT);
		Assert.assertTrue(bIsSendButtonExists,"Send button doesn't exist");
	}
	
	/**
	 * Purpose- To enter email address,subject and mail text in To,Subject and Body fields 
	 * @param filldata
	 * @throws Exception
	 */
	@Step("Entering values into To,Subject and Body Fields")
	public void enterTo_Subject_BodyFields(SendMailData filldata)
	{

		fillSendMaildata(filldata,con.getProperty("BrowserName"));

	}
	
	
	/**
	 * Purpose- To click on send button
	 * @throws Exception
	 */
	@Step("Clicking on Send Button")
	public void clickSendButton()
	{
		safeClick(SEND_BTN,"'Send' button in 'Compose' page", SHORTWAIT);
	}
	
	/**
	 * Purpose- To verify whether an email is sent successfully or not
	 * @throws Exception
	 */
	@Step("Verifying Sent mail message text")
	public HomePage verifySentMailMessage()
	{

		if(con.getProperty("BrowserName").contains("mobile")){
			safeClick(MENU_ICON_AND, "'Menu' icon in mobile", MEDIUMWAIT);
			safeClick(SENT_MAIL_TAB_AND, "sent mail tab in mobile", MEDIUMWAIT);
			waitForPageToLoad(LONGWAIT);
			Assert.assertTrue(isElementPresent(SENT_MAIL_BOX_COUNT_AND,VERYLONGWAIT), "Sent mail box mail count is not increased after sending mails");
		}
		else
		{
		boolean bViewMailLink = isElementPresent(VIEWMSG_LINK, LONGWAIT);
		boolean bSentMsgSuccess = isElementPresent(MSG_SENT_MESSAGE);
		Assert.assertTrue(bViewMailLink,"'View Message' link is not being displayed on clicking 'Send' button");
		Assert.assertTrue(bSentMsgSuccess,"The verification message- 'Your message has been sent' is not being displayed on clicking 'Send' button");
		}
		return new HomePage(driver);
	}
	
	/**
	 * Purpose- To click on send button and to verify saved message,saved button
	 */
	@Step("Clicking on Save Button")
	public HomePage clickSaveButton() 
	{
		if(con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			safeClick(CLOSE_BTN_COMPOSE_AND, "close the compose mail", MEDIUMWAIT);
			safeClick(SAVE_BTN_COMPOSE_AND, "save the composed mail", MEDIUMWAIT);
		}
		else
		{
		safeClick(SAVENOW_BTN,"'Save' button in 'Compose' page" ,SHORTWAIT);
		}
		return new HomePage(driver);
	}
	
	
	/**
	 * Purpose - To fill data in the send mail fields by calling from set and get methods
	 * @param datafields
	 * @throws Exception
	 */
	
	public void fillSendMaildata(SendMailData datafields,String browser_Name)
	{	


		if(con.getProperty("BrowserName").contains("mobile")){
			safeType(TO_FIELD_AND, datafields.getsToAddress(),"'To' input field in 'Compose' page ", SHORTWAIT);
			safeType(SUBJECT_FIELD_AND, datafields.getsSubject(),"'Subject' input field in 'Compose' page ", SHORTWAIT);
			safeJavaScriptType(BODYFRAME_AND, datafields.getsMessageBody(), "'Body' field in 'Compose' page ",SHORTWAIT);
		}
		else{
			safeType(TO_FIELD, datafields.getsToAddress(),"'To' input field in 'Compose' page ", SHORTWAIT);
			safeClearAndType(SUBJECT_FIELD, datafields.getsSubject(),"'Subject' input field in 'Compose' page ", SHORTWAIT);

		if(browser_Name.equalsIgnoreCase("iexplorer"))
		{
		safeTypeUsingRobot(BODYFRAME, datafields.getsMessageBody(), "lowercase", "", SHORTWAIT);
		}

		else{
			waitForSecs(3);
			safeType(BODYFRAME, datafields.getsMessageBody(), "'Body' field in 'Compose' page ",SHORTWAIT);
			waitForSecs(3);
		}
			defaultFrame();  

		}
	}
	/**
	 * purpose - To send email with datafields and to set properties to app.properties file
	 *
	 */
	public void fillMaildata(SendMailData datafields,String browser_Name)
	{
	    fillSendMaildata(datafields,browser_Name);
	    safeClick(SEND_BTN,"'Send' button in 'Compose' page", SHORTWAIT);
	   writeProperty();
	    
       
	}
	
	/**
	 * purpose - to set properties to app.properties file
	 */
	private void writeProperty()
	{
		 app.writeProperty("MAILSUBJECT1", app.getProperty("App.Subject1"));
		 System.out.println("sub1:"+app.getProperty("MAILSUBJECT1"));
	        app.writeProperty("MAILSUBJECT3", app.getProperty("App.Subject"));
	       System.out.println("sub3:"+app.getProperty("MAILSUBJECT3"));
	        app.writeProperty("MAILSUBJECT2", app.getProperty("App.Subject"));
	       System.out.println("sub2:"+app.getProperty("MAILSUBJECT2"));
	}


}

