package com.page.module;



import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Step;

import com.page.locators.ContactsLocators;
import com.selenium.Dynamic;
import com.selenium.SafeActions;
//import org.testng.Assert;
import com.testng.Assert;


public class ContactsPage extends SafeActions implements ContactsLocators
{
	private WebDriver driver;
	
	//Constructor to define/call methods	 
	public ContactsPage(WebDriver driver)
	{		
		super(driver);
		this.driver = driver;
    } 

	
	/**
	 * Purpose- To verify whether contacts page is being displayed or not
	 * @throws Exception
	 */
	@Step("Verifying contacts page")
	public void verifyContactsPage() 
	{
		switchToWindow(1);
	     waitUntilElementDisappears(LOADING,"'Loading....' Element in 'Contacts' page");
		boolean bContactsPage =  isElementPresent(ADDCONTACTS_BTN, MEDIUMWAIT);
		Assert.assertTrue(bContactsPage,"Add contacts button is not being displayed/recognized on contacts page");
	}
	/**
	 * Purpose- To add contacts from contact page
	 * @param sContactName- we pass contact name
	 * @throws Exception
	 */
	@Step("Adding contacts from Contacts page")
	public void addContacts(String sContactName) 
	{	
		safeActionsClick(ADDCONTACTS_BTN,"'Add New Contact' button in 'Contacts' page ",VERYLONGWAIT);
		if(isElementVisible(CONTACT_TXTAREA,VERYLONGWAIT))
		safeType(CONTACT_TXTAREA, sContactName,"'Contact Text Area' field in 'Contacts' page",VERYLONGWAIT);
		safeClick(CONTACT_TXTAREA,"'Contact Text Area' field in 'Contacts' page",VERYLONGWAIT);
		safeClick(CREATE_BTN,"'Create' button in 'Contacts' page",VERYLONGWAIT);
		safeClick(SAVE_BTN,"'Save' button in 'Contacts' page",VERYLONGWAIT);
		verifyAddedContacts();
       
        refresh();
  
	}
	
	
	/**
	 * Purpose- To delete the contacts from contact page
	 * @param - sContactName Name of the contact to be deleted
	 * @throws Exception
	 */
	@Step("Deleting contacts from contacts page")
	public HomePage deleteContacts(String sContactName) 
	{
		waitForPageToLoad();
		safeActionsClick(Dynamic.getNewLocator(TXT,sContactName),"'Text'",VERYLONGWAIT);
		waitForPageToLoad();
		safeActionsClick(MOREACTIONS_LNK,"'More Actions' link in 'Contacts' page ",VERYLONGWAIT);
        safeActionsClick(DELETE,"'Delete' link in 'Conatacts' page ",VERYLONGWAIT);
     
		verifyDeletedContacts();
		waitForPageToLoad();
	
        driver.close();
        switchToWindow(0);
     
        
	   return new HomePage(driver);
	}
	/**
	 * purpose-to verify whether contact is added or not
	 */
	
	public void verifyAddedContacts()
	{
		boolean b=isElementPresent(CONTACTSSAVED,VERYLONGWAIT);
	Assert.assertTrue(b,"contact is not added");
	}
	/**
	 * purpose-to verify whether contact is deleted or not
	 */
	public void verifyDeletedContacts()
	{
		boolean b=isElementPresent(DELETED_MSG,VERYLONGWAIT);
		Assert.assertTrue(b,"contact is not deleted");
	}
	
	
	
	
}
