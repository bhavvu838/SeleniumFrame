package com.page.module;

import org.openqa.selenium.WebDriver;

import ru.yandex.qatools.allure.annotations.Step;

import com.datamanager.ConfigManager;
import com.page.locators.InboxLocators;
import com.selenium.Dynamic;
import com.selenium.SafeActions;
//import org.testng.Assert;
import com.testng.Assert;


public class InboxPage extends SafeActions implements InboxLocators
{
	private WebDriver driver;
	ConfigManager con;
	
	//Constructor to define/call methods	 
	public InboxPage(WebDriver driver) 
	{		
		super(driver);
		this.driver = driver;
		con = new ConfigManager();
    } 



	/**
	 * Purpose- To click on email with specified subject
	 * @param sSubject - We pass the Subject of the mail 
	 * @throws Exception
	 */
	@Step("Clicking on email from Inbox page")
	public void clickMail(String sSubject) 
	{
		if(con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			Assert.assertTrue(isElementPresent(PRIMARY_TITLE), "primary title is not displayed in inbox page");
		}
		else
		{
		waitUntilElementDisappears(LOADING, "'Loading....' Element in 'Inbox' page",MEDIUMWAIT);
		boolean bMail = isElementPresent(Dynamic.getNewLocator(EMAIL, sSubject),MEDIUMWAIT);
		Assert.assertTrue(bMail,"Unread Email with specified subject is not being displayed in 'Inbox'");
		safeClick(Dynamic.getNewLocator(EMAIL, sSubject),"'Email' link in 'Inbox' page",MEDIUMWAIT);
		}
	}
	
	/**
	 * Purpose- To verify opened mail with specified subject and body
	 * @param sSubject- we pass the subject of the mail
	 * @param sBody- we pass the body text of the mail
	 * @throws Exception
	 */
	@Step("Verifying opened mail page")
	public HomePage verifyOpenedMail(String sSubject, String sBody) 
	{
		if(con.getProperty("BrowserName").equalsIgnoreCase("mobileChrome")){
			Assert.assertTrue(isElementPresent(PRIMARY_MAIL_COUNT), "primary title is not displayed in inbox page");
		}
		else
		{
		boolean bSubject = isElementPresent(Dynamic.getNewLocator(EMAILSUBJECT, sSubject), MEDIUMWAIT);
		Assert.assertTrue(bSubject,"Opened email doesn't have the specified subject");
		String emailBody=safeGetText(EMAILBODY,"'Email Body' field in 'Inbox' page",SHORTWAIT);
		Assert.assertEquals(emailBody,sBody,"Opened email with specified subject doesn't have expected body information..");
		}
		return new HomePage(driver);
	}
}
