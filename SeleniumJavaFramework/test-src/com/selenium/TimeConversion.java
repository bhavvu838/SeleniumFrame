package com.selenium;

import java.io.BufferedReader;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import java.nio.file.Files;
import java.nio.file.Path;

public class TimeConversion {

	
	public static String fileSeparator = System.getProperty("file.separator");



	public static void main(String[] args) {
	
	
		FileReader fr = null;
        FileWriter fw = null;



        String fOverviewPath = System.getProperty("user.dir") + fileSeparator + "Automation Reports" + fileSeparator + "Reports" + fileSeparator + "html" + fileSeparator + "overview.html";
        String fTimeConvertedPath = System.getProperty("user.dir") + fileSeparator + "Automation Reports" + fileSeparator + "Reports" + fileSeparator + "html" + fileSeparator + "TimeConverted.html";
        File fOverview = new File(fOverviewPath);
        File fTimeConverted = new File(fTimeConvertedPath);
        
        
        
	        try {
	            fr = new FileReader(fOverview);
	            fw = new FileWriter(fTimeConverted);


	            
	            BufferedReader br = new BufferedReader(fr);
	            String line;
	            while((line = br.readLine())!= null){
	            	if(line.contains("class=\"duration\"")){
	            		fw.write(line+"\n");
	            		fw.write(timeConversion(br.readLine())+"\n");
	            	}else{
	            		fw.write(line+"\n");
	            	}
	           }
	       } catch(IOException e) {
	            e.printStackTrace();
	       } finally {
	            close(fr);
	            close(fw);
	       }


	        
	        fOverview.delete();
	        Path source = fTimeConverted.toPath();
	        try {
				Files.move(source, source.resolveSibling("overview.html"));
			} catch (IOException e) {
				
				e.printStackTrace();
			}

        }
	
	
	public static void close(Closeable stream) {
        try {
            if (stream != null) {
                stream.close();
            }
        } catch(IOException e) {
           //log.error(e.getMessage());
        }
    }
	
	public static String timeConversion(String s2){
		
		String s1 = s2.split("s")[0].trim();
		double d = Double.parseDouble(s1);	
		int t = (int) d;
				
		if(t < 60){
			return "0H:0M:"+t+" S";
		}else if(t >= 60 && t < 3600){
			int minu = t/60;
			int s = t%60;
			return "0H:"+minu+"M:"+s+" S";
		}else{
			int hours =t/3600;
			int minu =(t%3600)/60;
			int s = ((t%3600)%60);
			return hours+"H:"+minu+"M:"+s+" S";
		}
	}

}
